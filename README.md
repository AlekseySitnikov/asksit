# README #

Ситников Алексей. АПО-13. Технопарк.
ДЗ по веб-разработке.

### **Домашнее задание №1** ###
* [Директория проекта django.](https://bitbucket.org/AlekseySitnikov/asksit/src/7e203060c7a5ba69e20a6f995c91430d383ffd8e/askSit/)
* [Результаты нагрузочного тестирования.](https://bitbucket.org/AlekseySitnikov/asksit/src/7e203060c7a5ba69e20a6f995c91430d383ffd8e/ab-output/)
* [Конфиг apache2](https://bitbucket.org/AlekseySitnikov/asksit/src/7e203060c7a5ba69e20a6f995c91430d383ffd8e/apache2.conf)
* [Конфиг nginx](https://bitbucket.org/AlekseySitnikov/asksit/src/7e203060c7a5ba69e20a6f995c91430d383ffd8e/nginx.conf)

### **Домашнее задание №2** ###
* [Шаблоны страниц](https://bitbucket.org/AlekseySitnikov/asksit/src/5f2eb360892d23fb219165c7291bdcaa9fbd26ac/askSit/templates/)
* [Джанго приложение](https://bitbucket.org/AlekseySitnikov/asksit/src/5f2eb360892d23fb219165c7291bdcaa9fbd26ac/askSit/worker/)