from django.http import HttpResponse
from django.shortcuts import render_to_response
from django.views.decorators.csrf import csrf_exempt

@csrf_exempt

def index(request):
	return render_to_response('index.html')

def signup(request):
	return render_to_response('signup.html')

def login(request):
	return render_to_response('login.html')
