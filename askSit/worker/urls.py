from django.conf.urls import patterns, url
from worker import views

urlpatterns = patterns('',
	url(r'^$', views.index),
	url(r'^signup/$', views.signup),
	url(r'^login/$', views.login),
	)