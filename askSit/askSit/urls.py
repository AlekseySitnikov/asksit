from django.conf.urls import patterns, include, url
from django.contrib import admin

urlpatterns = patterns('',
	#url(r'^admin/', admin.site.root),
	url(r'^hello/$', include('hello.urls')),
	url(r'', include('worker.urls')),
	)
