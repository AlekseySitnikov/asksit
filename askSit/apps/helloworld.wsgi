from cgi import parse_qs

def application(environ, start_response):
	response_body = []
	response_body.append('Hello wsgi & apache!\n')

	response_body.append(environ['REQUEST_METHOD'] + '\n')
	
	if environ['REQUEST_METHOD'] == 'GET':
		request_body = parse_qs(environ['QUERY_STRING'])
		for keys, values in request_body.items():
			response_body.append('%s =\t\t%s\n' % (keys, values))

	elif environ['REQUEST_METHOD'] == 'POST':
		try:
			request_body_size = int(environ.get('CONTENT_LENGTH', 0))
		except (ValueError):
			request_body_size = 0

		request_body = parse_qs(environ['wsgi.input'].read(request_body_size))
		for keys, values in request_body.items():
			response_body.append('%s =\t\t%s\n' % (keys, values))

	response_headers = [ ('Content-type', 'text/plain'),
						('Content-Length', str(sum(len(line) for line in response_body))) ]
	status = '200 OK'
	start_response(status, response_headers)

	return response_body