from django.http import HttpResponse
from django.views.decorators.csrf import csrf_exempt

@csrf_exempt

def index(request):
	response = HttpResponse(
		#content_type = 'text/plain'
		)
	response.write('Hello python & django!<br>')

	response.write('%s<br>' % request.method)
	if request.method == 'GET':
		for keys, values in request.GET.dict().items():
			response.write('%s =\t\t%s<br>' % (keys, values))

	elif request.method == 'POST':
		for keys, values in request.POST.dict().items():
			response.write('%s =\t\t%s<br>' % (keys, values))

	return response